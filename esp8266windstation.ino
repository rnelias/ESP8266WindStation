#include <FS.h> //this needs to be first, or it all crashes and burns...

#include <Wire.h>   // for I2C devices
#include <LiquidCrystal_I2C.h>
#include <ESP8266WiFi.h>
#include "PubSubClient.h"
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <WiFiManager.h> //https://github.com/tzapu/WiFiManager
#include <DNSServer.h> 
#include <ArduinoJson.h> //https://github.com/bblanchon/ArduinoJson
#include "TimeLib.h"
#include <Ticker.h>  //Ticker Library to call a function from time to time
#include <DallasTemperature.h>
#include "defines.h"

// WEB OTA
//#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>

ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;
const char* host = "windstation-webupdate";
// para atualizar, abra o browser, aponte para http://windstation-webupdate.local/update e faça a atualização por lá

#define VERBOSE 0

#if VERBOSE
#define debugln(x) Serial.println(x)
#define debug(x) Serial.print(x)
#else
#define debugln(x)
#define debug(x)
#endif

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

// --- Modulo LCD
#define HAS_LCD_OUTPUT 0

#if HAS_LCD_OUTPUT
#define endereco 0x27
#define colunas 20
#define linhas 4
LiquidCrystal_I2C lcd(endereco, colunas, linhas);
#endif


// Atribuicao de pinos
#define SOFT_SDA     D1               // for i2c devices
#define SOFT_SCL     D2               // for i2c devices
#define DS18B20_Pin  D7               // DS18S20 Signal
#define WIFI_LED     D8               // led de status de conexão WiFi (verde)
#define MQTT_LED     D6               // led de status de conexão MQTT (laranja)
#define WINDPIN      D5               // sensor de velocidade do vento

#define BUTTON      4                 // optional, GPIO4 for Witty Cloud. GPIO0/D3 for NodeMcu (Flash) - not work with deepsleep, set 4!
#define LED         2                 // GPIO2 for Witty Cloud. GPIO16/D0 for NodeMcu - not work with deepsleep, set 2!


#define USE_Windguru
#define USE_Windy_com
//#define USE_Windy_app

OneWire oneWire(DS18B20_Pin);
DallasTemperature sensors(&oneWire);

String st;
String content;
int statusCode;

int debouncing_time = 1000;             // time in microseconds!

unsigned int t1 = 0;                    // armazenam os tempos das ultimas chamadas das rotinas SendToWindGuru
unsigned int t2 = 0;                    // armazenam os tempos das ultimas chamadas das rotinas SendToWindy

extern "C" { 
  #include "user_interface.h" 
}

bool sendStatus        = true;
#define Has_DS18B20       true
#define Has_WindDirection true
#define Has_Anemometer    true

int requestRestart   = 0;
int kRetries         = 10;                                  // numero de tentativas de conexao no WIFI
int SamplingCounter  = 0;
int calDirection     = 0;                                   // calibrated direction of wind vane after offset applied

const float kKnots = 1.94;                                  // m/s to knots conversion   
const int windPeriodSec = 30;                               // wind measurement period in seconds 1-10sec

float TempValue;
float windMS  = 0;
float WindMax = 0; 
float WindAvr = 0; 
float WindMin = 0; 

String str;
String Windir;

unsigned long count_btn = 0;

WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);
Ticker btn_timer;

//=========================================================================================================================

void callback(const MQTT::Publish& pub) {
  debugln("MQTT Topic: " + pub.topic() + " MQTT Payload: " + pub.payload_string());
  if (pub.payload_string() == "stat") {
  }
  else if (pub.payload_string() == "reset") {
    requestRestart = 1;
  }
  else if (pub.payload_string() == "sensor") {
    if (minute()<10)
      mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/debug",String(hour()) + ":0" + String(minute())).set_retain().set_qos(1));
    else
      mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/debug",String(hour()) + ":" + String(minute())).set_retain().set_qos(1));  
    //sensorReport = true;
  }
  else if (pub.payload_string() == "adc") {
     mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/debug","ADC:" + String(analogRead(A0))).set_retain().set_qos(1));   
  }
  else if (pub.payload_string() == "flash") {
     noInterrupts();
     WiFiClient client;
     t_httpUpdate_return ret = ESPhttpUpdate.update(client, FirmwareURL);

    switch (ret) {
      case HTTP_UPDATE_FAILED:
        Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
        mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/debug", ESPhttpUpdate.getLastErrorString().c_str()).set_retain().set_qos(1));
        break;
      case HTTP_UPDATE_NO_UPDATES:
        debugln("HTTP_UPDATE_NO_UPDATES");
        mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/debug", "HTTP_UPDATE_NO_UPDATES").set_retain().set_qos(1));
        break;
      case HTTP_UPDATE_OK:
        debugln("HTTP_UPDATE_OK");
        break;
     }
  }
  else { 
    str = pub.payload_string();
    int i = atoi(str.c_str());
    if ((i >= 0) && (i < 9999)) {
      if (pub.topic() == MQTT_TOPICo) {
        SpeedRatio = str.toFloat(); 
        debugln(" Changind wind speed correction ratio to: "+String(SpeedRatio));
      }

      mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/debug","saving config").set_retain().set_qos(1));
      debugln("saving config");
      DynamicJsonDocument json(1024);
      json["mqtt_server"] = mqtt_server;
      json["mqtt_port"] = mqtt_port;
      json["mqtt_user"] = mqtt_user;
      json["mqtt_pass"] = mqtt_pass;
      json["windguru_uid"] = windguru_uid;
      json["windguru_pass"] = windguru_pass;
      json["vaneMaxADC"] = vaneMaxADC;
      json["vaneOffset"] = buf_vo; // vaneOffset;
      json["SpeedRatio"] = buf_sr; // SpeedRatio;
    
      File configFile = SPIFFS.open("/config.json", "w");
      if (!configFile) {
        debugln("failed to open config file for writing");
      }

      serializeJson(json, Serial);
      serializeJson(json, configFile);
      configFile.close();
      
      sendStatus = true;
    }
  }
}

//flag for saving data
bool shouldSaveConfig = false;

//callback notifying us of the need to save config
void saveConfigCallback () {
  debugln(" Should save config ");
  shouldSaveConfig = true;
}


//=========================================================================================================================

// This is the function that the interrupt calls to increment the rotation count 
void ICACHE_RAM_ATTR rpm_anemometer(){

    if ((millis() - last_micros) > 15 ) { // debounce the switch contact. 
    rpmcount++; 
    last_micros = millis(); 
  } 
}

//                                           * * *    S E T U P    * * * 
//=========================================================================================================================

void setup() {

  // Inicializando porta serial
  Serial.begin(115200);
  debug("\n\n ======================== Wind Station "); 
  debug(VERSION);
  debug("======================== \n\n");
  debug("\nESP ChipID: ");
  debugln(ESP.getChipId());

  // Inicializando LEDs
  pinMode(WIFI_LED,OUTPUT); // LED de status da conexão WiFi
  pinMode(MQTT_LED,OUTPUT); // LED de status do MQTT

  // INICIANDO LCD EXTERNO
  InitLCD();

  //clean FS, erase config.json in case of damaged file
  //SPIFFS.format();

  // Leitura de arquivo config.json armazenado na memória flash do ESP8266
  debugln("mounting FS...");

  if (SPIFFS.begin()) {
    debugln("mounted file system");
    if (SPIFFS.exists("/config.json")) {
        //file exists, reading and loading
        debugln("reading config file");
        File configFile = SPIFFS.open("/config.json", "r");
        if (configFile) {
        debugln("opened config file");

        DynamicJsonDocument json(1024);
        DeserializationError error = deserializeJson(json, configFile);
        if (error) {
           debug(F("deserializeJson() failed with code "));
           debugln(error.c_str());
        } else {
          serializeJson(json, Serial);
          debugln("\nparsed json");
          strcpy(mqtt_server, json["mqtt_server"]);
          strcpy(mqtt_port, json["mqtt_port"]);
          strcpy(mqtt_user, json["mqtt_user"]);
          strcpy(mqtt_pass, json["mqtt_pass"]);
          strcpy(buf_vo, json["vaneOffset"]); // ##
          strcpy(buf_sr, json["SpeedRatio"]); // ##
        }
      }
    }
  } else {
    debugln("failed to mount FS");
  }
  //end read configuration

  // The extra parameters to be configured (can be either global or just in the setup)
  // After connecting, parameter.getValue() will get you the configured value
  // id/name placeholder/prompt default length
  WiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 30);
  WiFiManagerParameter custom_mqtt_port("port", "mqtt port", mqtt_port, 6);
  WiFiManagerParameter custom_mqtt_user("user", "mqtt username", mqtt_user, 20);
  WiFiManagerParameter custom_mqtt_pass("pass", "mqtt password", mqtt_pass, 20);
  WiFiManagerParameter custom_vaneOffset("vaneOffset", "Wind direction correction factor", buf_vo, 5); //##
  WiFiManagerParameter custom_SpeedRatio("SpeedRatio", "Anemometer correction factor 0-1", buf_sr, 5); //##

  //pinMode(LED, OUTPUT);
  
  //firstRun = true;
  btn_timer.attach(0.05, button);

  mqttClient.set_callback(callback);

  WiFiManager wifiManager;
  wifiManager.setSaveConfigCallback(saveConfigCallback);   //set config save notify callback
  wifiManager.setTimeout(60);

  //add all your parameters here
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_mqtt_user);
  wifiManager.addParameter(&custom_mqtt_pass);
  wifiManager.addParameter(&custom_vaneOffset);
  wifiManager.addParameter(&custom_SpeedRatio);

  debugln(" >>> Trying to connect to SSID: "+String(NameAP));
  if(!wifiManager.autoConnect(NameAP, PasswordAP)) {
    debugln("failed to connect and hit timeout");
    delay(5000);
  }
  else {
    debugln("CONNECTED!");
  }

  //read updated parameters
  strcpy(mqtt_server, custom_mqtt_server.getValue());
  strcpy(mqtt_port, custom_mqtt_port.getValue());
  strcpy(mqtt_user, custom_mqtt_user.getValue());
  strcpy(mqtt_pass, custom_mqtt_pass.getValue());
  strcpy(buf_vo, custom_vaneOffset.getValue()); //##
  strcpy(buf_sr, custom_SpeedRatio.getValue()); //##

  
  //save the custom parameters to FS
  if (shouldSaveConfig) {
    debugln("saving config");
    DynamicJsonDocument json(1024);
    json["mqtt_server"] = mqtt_server;
    json["mqtt_port"] = mqtt_port;
    json["mqtt_user"] = mqtt_user;
    json["mqtt_pass"] = mqtt_pass;
    json["vaneOffset"] = buf_vo;
    json["SpeedRatio"] = buf_sr;
    
    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      debugln("failed to open config file for writing");
    }

    serializeJson(json, Serial);
    serializeJson(json, configFile);
    configFile.close();
    //end save parameters
  
  }  
  
  debug("\nConnecting to WiFi"); 
  while ((WiFi.status() != WL_CONNECTED) && kRetries --) {
    delay(500);
    debug(" .");
  }
  if (WiFi.status() == WL_CONNECTED) {
    digitalWrite(WIFI_LED, HIGH); // liga LED de status do WiFi (VERDE)
    debugln(" DONE");
    debug("IP Address is.: "); debugln(WiFi.localIP());
    debug("MAC Address is: "); debugln(WiFi.macAddress());
    debug("RSSI is.......: "); debugln(WiFi.RSSI());
    debug("Connecting to : "); debug(mqtt_server);debug(" Broker . .");
    debug("mqtt user.....: "); debug(mqtt_user);debug(" mqtt pwd: "); debug(mqtt_pass);
    delay(500);

    // MQTT server connection
    mqttClient.set_server(mqtt_server, atoi(mqtt_port));
   
    while (!mqttClient.connect(MQTT::Connect(String(ESP.getChipId(), HEX)).set_keepalive(90).set_auth(mqtt_user, mqtt_pass)) && kRetries --) {
      debug(" .");
      delay(1000);
    }
    if(mqttClient.connected()) {
      digitalWrite(MQTT_LED, HIGH); // liga led de status do MQTT
      debugln(" MQTT connection DONE");
      debugln("\n----------------------------  Logs  ----------------------------");
      debugln();
      mqttClient.subscribe(MQTT_TOPIC);
      mqttClient.subscribe(MQTT_TOPICm);
      mqttClient.subscribe(MQTT_TOPICo);
      mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/debug", "Compiled: " __DATE__ " / " __TIME__).set_retain().set_qos(1));
    }
    else {
    digitalWrite(MQTT_LED, LOW); // desliga led de status do MQTT
      debugln(" MQTT connection FAILED!");
      debugln("\n----------------------------------------------------------------");
      debugln();
    }
  }
  else {
    digitalWrite(WIFI_LED, LOW); // desliga led de status do WiFi (VERDE)
    debugln(" WiFi FAILED!");
    debugln("\n----------------------------------------------------------------");
    debugln();
  }

  // Obtem o nome da rede (SSID) e a senha para usar no OTA
  //debugln(WiFi.SSID());
  //debugln(WiFi.psk());

  // WSEB OTA
  MDNS.begin(host);
  httpUpdater.setup(&httpServer);
  httpServer.begin();

  MDNS.addService("http", "tcp", 80);
  Serial.printf("HTTPUpdateServer ready! Open http://%s.local/update in your browser\n", host);
  // END WEB OTA

  sensors.begin();

  vaneOffset = atof(buf_vo);
  SpeedRatio = atof(buf_sr);
  debugln( " >> Wind direction correction factor.: "+String(buf_vo));
  debugln( " >> Wind speed correction factor.....: "+String(buf_sr));  

  //
  // INICIANDO ANEMOMETRO (velocidade do vento)
  //
  pinMode(WINDPIN, INPUT_PULLUP);
  digitalWrite(WINDPIN, LOW);
  detachInterrupt(WINDPIN);  // force to initiate Interrupt on zero  
  attachInterrupt(WINDPIN, rpm_anemometer, RISING);   // conecta interrupção do pino WINDPIN à rotina rpm_anemometer
  rpmcount = 0;
  rpm = 0;
  timeold = 0;

  ResetAnemometer();

}

//=========================================================================================================================
//                                      L I F E     C Y C L E     L O O P
//=========================================================================================================================

void loop() { 


  httpServer.handleClient(); // WEB OTA
  MDNS.update(); // WEB OTA
  mqttClient.loop();
  checkStatus();
  ReadSensors();
  
  // Check frequency to send data to weather websites
  if (millis() - t1 >=  SendDataToSites*1000) {
     debugln("\n >>> Sending data do WindGuru.com... ");    
     SendToWindguru();    
     debugln("\n >>> Sending data do Windy.com... ");    
     SendToWindyCom();
     t1 = millis();
     ResetAnemometer();         
  }
}

//=========================================================================================================================

void button() {
  if (!digitalRead(BUTTON)) {
    count_btn++;
  } 
  else {
    if (count_btn > 1 && count_btn <= 40) {   
      sendStatus = true;
    } 
    else if (count_btn > 40){
      debugln("\n\nESP8266 Rebooting . . . . . . . . Please Wait"); 
      requestRestart = 1;
    } 
    count_btn=0;
  }
}

//=========================================================================================================================

void checkConnection() {
  if (WiFi.status() == WL_CONNECTED)  {
    debugln("WiFi connection . . . . . . . . . . OK");
    digitalWrite(WIFI_LED, HIGH); // liga led de status do WIFI
    if (mqttClient.connected()) {
      debugln("mqtt broker connection . . . . . . . . . . OK");
      digitalWrite(MQTT_LED, HIGH); // liga led de status do MQTT
    } 
    else {
      debugln("mqtt broker connection . . . . . . . . . . LOST");
      if (requestRestart < 1) requestRestart = 60;
      digitalWrite(MQTT_LED, LOW); // desliga led de status do MQTT      
    }
  }
  else { 
    debugln("WiFi connection . . . . . . . . . . LOST");
    digitalWrite(WIFI_LED, LOW); // desliga led de status do WIFI
    if (requestRestart < 1) requestRestart = 5;
  }
}

//=========================================================================================================================
//                             M Q T T       F U N C T I O N S
//=========================================================================================================================

void checkStatus() {
  if (sendStatus) {
    mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/adc", "{\"ADC\":" + String(analogRead(A0)) +", "+"\"MaxADC\":" + String(vaneMaxADC) + ", " + "\"SpeedRatio\":"+String(SpeedRatio)+ "}").set_retain().set_qos(1)); 
    sendStatus = false;
  }
  if (requestRestart == 1) {
#if HAS_LCD_OUTPUT
    lcd.clear();
#endif
    digitalWrite(MQTT_LED, LOW);
    digitalWrite(WIFI_LED, LOW);
    ESP.restart();
    delay(500);
  }
}

//===============================================================================
//                       Update MQTT
//===============================================================================
void UpdateMQTT() {
   char message_buff[60];
   String pubString;
   pubString = "{\"Min\": "+String(WindMin*kKnots, 2)+", "+"\"Avr\": "+String(WindAvr*kKnots, 2)+", "+"\"Max\": "+String(WindMax*kKnots, 2)+", "+"\"Dir\": "+String(calDirection) + "}";
   pubString.toCharArray(message_buff, pubString.length()+1);
   if (mqttClient.connected())
      mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/wind", message_buff).set_retain().set_qos(1));
   debug("[MQTT update] Wind Min: " + String(WindMin*kKnots, 2) + "k, Avr: " + String(WindAvr*kKnots, 2) + "k, Max: " + String(WindMax*kKnots, 2) + "k, Dir: " + String(calDirection)+ " \n");
}

//=========================================================================================================================
//                             L E I T U R A      D O S      S E N S O R E S
//=========================================================================================================================
void ReadSensors() {

    if ((millis() - timeold) >=  timemeasure*1000){ 

        debugln( "\n\n\n (ReadSensors) reading sensors... ");
    
        // SENSOR DS18B20 (Temperatura)
        //============================================================================        
        if (Has_DS18B20) ReadDS18B20();
   
        // ANEMOMETRO (PINO D1)
        //============================================================================
        if (Has_Anemometer) ReadWindSpeed();

        // BIRUTA (PINO A0)
        //============================================================================
        if (Has_WindDirection) ReadWindDirection();

        // UPDATE MQTT DATA
        //============================================================================
        if (SamplingCounter > 0) UpdateMQTT();
    
        timeold = millis();

        // WRITE DATA TO EXTERNAL LCD
        //============================================================================
        WriteExternalLCD();
    
    }

}

//===============================================================================
//                 SENSOR DE TEMPERATURA
//===============================================================================
void ReadDS18B20()
{
    sensors.requestTemperatures();
    TempValue = sensors.getTempCByIndex(0); 
    debug("\t Temperature = "+String(TempValue,2)+" Celsius\n ");
}

//===============================================================================
//                 SENSOR DE DIREÇÃO DE VENTO
//===============================================================================
void ReadWindDirection(){

    int VaneValue = analogRead(A0);
    int Direction = map(VaneValue, 0, 1023, 0, 360);
    calDirection = Direction + vaneOffset;
    //debugln(" DIRECAO: "+String(Direction)+"  Offset: "+vaneOffset+" calDirection: "+String(calDirection));
    
    if(calDirection > 360)
    calDirection = calDirection - 360;
    
    if(calDirection < 0)
    calDirection = calDirection + 360;

   debug( "\t Wind direction: "+String(calDirection)+"  - Analog reading: "+String(VaneValue)+" ==> ");
   getHeading(calDirection);
   debugln(" \t Wind direction correction:"+String(vaneOffset,2)+"   Speed wind correction: "+String(SpeedRatio,2));
}

//===============================================================================
//                 CONVERTE ANGULO EM DIRECAO
//===============================================================================

// Converts compass direction to heading
void getHeading(int direction) {

    if(direction < 22)
    Windir = "N";
    else if (direction < 67)
    Windir = "NE";    
    else if (direction < 112)
    Windir = "E";    
    else if (direction < 157)
    Windir = "SE";    
    else if (direction < 212)
    Windir = "S";    
    else if (direction < 247)
    Windir = "SW";    
    else if (direction < 292)
    Windir = "W";    
    else if (direction < 337)
    Windir = "NW";    
    else
    Windir = "N";

    debugln(Windir);
}

//===============================================================================
//                     SENSOR DE VELOCIDADE DE VENTO
//===============================================================================
void ReadWindSpeed() {
  
   //Measure RPM
   detachInterrupt(WINDPIN);                    // Disable interrupt when calculating
   rps = 2*float(rpmcount)/float(timemeasure);  // rotations per second
   omega = 2*PI*rps;                            // rad/seg
   linear_velocity = omega*radius*SpeedRatio;   // m/s
   windMS  = linear_velocity;
   
   if (windMS > 0) {
       SamplingCounter ++;    
       WindAvr = windMS;
       WindMax = MAX(WindMax,linear_velocity);
       WindMin = MIN(WindMin,linear_velocity);
   }

   debugln("\t Count.............: "+String(SamplingCounter));
   debugln("\t RPM count.........: "+String(rpmcount));
   debugln("\t Sampling time.....: "+String(timemeasure)+" seconds" );
   debugln("\t RPS...............: "+String(rps));
   debugln("\t Vel avg (m/s).....: "+String(WindAvr));
   debugln("\t Vel avg (knots:)..: "+String(WindAvr*kKnots));

   rpmcount = 0;
  
  //if (SamplingCounter > 0) UpdateMQTT();
                 
   attachInterrupt(WINDPIN, rpm_anemometer, FALLING); // enable interrupt
  
}

//===============================================================================
//                    Reset Anemometer Counters
//===============================================================================
void ResetAnemometer() {

   debugln( " Reseting anemometer counters... ");
   SamplingCounter = 0;
   windMS     = 0;
   WindMax    = 0;
   WindMin    = 10;
   WindAvr    = 0;
   SamplingCounter  = 0;
 
}

//===============================================================================
void InitLCD() {
#if HAS_LCD_OUTPUT
  //lcd.init();
  lcd.begin();
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("WindStation ");
  lcd.print(VERSION);
  lcd.setCursor(0,1);
  lcd.print(compile_date);
  lcd.setCursor(0,2);
  lcd.print("SSID: "); lcd.print(NameAP);
  lcd.setCursor(0,3);
  lcd.print("PSWD: "); lcd.print(PasswordAP);
  delay(5000); // wait 5 seconds
#endif
}

//===============================================================================
void WriteExternalLCD() {
#if HAS_LCD_OUTPUT
    debug("Writing to external LCD");
    lcd.clear();    
    lcd.setCursor(0,0);
    lcd.print("ESTACAO BICA");
    lcd.setCursor(0,1);
    lcd.print("TEMP:"+String(TempValue,1)+" ");lcd.print(char(223));lcd.print("C");
    lcd.setCursor(0,2);
    lcd.print("MED:"+String(WindAvr*kKnots,2)+"k DIR: "+Windir);
    //lcd.print("KNOTS:"+String(WindAvr*kKnots)+"     ");
    lcd.setCursor(0,3);
    lcd.print("MIN:"+String(WindMin*kKnots,2)+"k MAX:"+String(WindMax*kKnots,2)+"k");
    //lcd.print("DIRECTION:"+Windir);
#endif
}

//==========================================================================================================================

bool SendToWindguru() { // send info to windguru.cz
  
  WiFiClient client;
  HTTPClient http; //must be declared after WiFiClient for correct destruction order, because used by http.begin(client,...)
  String getData, Link;
  unsigned long time;
  
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
     Link = "http://www.windguru.cz/upload/api.php?";
     time = millis();
     
     //--------------------------md5------------------------------------------------
     MD5Builder md5;
     md5.begin();
     md5.add(String(time) + String(windguru_uid) + String(windguru_pass));
     md5.calculate();
     debugln(md5.toString()); // can be getChars() to getBytes() too. (16 chars) .. see above 
     //--------------------------md5------------------------------------------------
     
     //wind speed during interval (knots)
     getData = "uid=" + String(windguru_uid) + "&salt=" + String(time) + "&hash=" + md5.toString() + "&wind_min=" + String(WindMin * kKnots, 2) + "&wind_avg=" + String(WindAvr * kKnots, 2) + "&wind_max=" + String(WindMax * kKnots, 2);
     //wind_direction     wind direction as degrees (0 = north, 90 east etc...) 
     getData = getData + "&wind_direction=" + String(calDirection);

     if (Has_DS18B20) {
        if (!isnan(TempValue)) getData = getData + "&temperature=" + String(TempValue);
     }
     
     debugln(Link + getData);
     
     http.begin(client, Link + getData);    //Specify request destination
     
     int httpCode = http.GET();             //Send the request
     if (httpCode > 0) {                    //Check the returning code
      
       String payload = http.getString();   //Get the request response payload
       //debugln(" >>>>> "+payload+" <<<<< ");             //Print the response payload
       if (mqttClient.connected() && (payload != "OK"))
          mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/debug",payload).set_retain().set_qos(1));

       // file found at server
       if(httpCode == HTTP_CODE_OK) {
          String payload = http.getString();
          debugln( " WindGuru http return code: "+String(httpCode)+" \n");
       }
        } else {
            Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());         
     }
     http.end();   //Close connection

   } else  {
      debugln("wi-fi connection failed");
      return false; // fail;
   }

  return true; //done
}

//https://stations.windy.com/pws/update/XXX-API-KEY-XXX?winddir=230&windspeedmph=12&windgustmph=12&tempf=70&rainin=0&baromin=29.1&dewptf=68.2&humidity=90  
bool SendToWindyCom() { // send info to http://stations.windy.com/stations
  WiFiClient client;
  HTTPClient http; //must be declared after WiFiClient for correct destruction order, because used by http.begin(client,...)
  String getData, Link;
  unsigned long time;
  
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status

     Link = "http://stations.windy.com/pws/update/" + WindyComApiKey + "?station=1";
    
     //wind speed during interval (knots)
     getData = "&winddir=" + String(calDirection) + "&wind=" + String(WindAvr, 2) + "&gust=" + String(WindMax, 2);

     if (Has_DS18B20) {
        if (!isnan(TempValue)) getData = getData + "&temp=" + String(TempValue);
     }
     
     debugln(" Sending data to Windy... ");
     debugln(Link + getData);
     http.begin(client, Link + getData);    // Specify request destination
     int httpCode = http.GET();             // Send the request
     debugln("Windy http return code: "+String(httpCode));
     if (httpCode > 0) {                    // Check the returning code
        String payload = http.getString();  // Get the request response payload
        debugln(payload);            // Print the response payload
        if (mqttClient.connected() && (payload != "SUCCESS"))
           mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/debug",payload).set_retain().set_qos(1));
     }
     
     http.end();   //Close connection
   } else  {
      debugln("wi-fi connection failed");
      return false; // fail;
   }
    
  return true; //done
}



//https://windyapp.co/apiV9.php?method=addCustomMeteostation&secret=WindyAppSecret&d5=123&a=11&m=10&g=15&i=test1
//d5* - direction from 0 to 1024. direction in degrees is equal = (d5/1024)*360
//a* - average wind per sending interval. for m/c - divide by 10
//m* - minimal wind per sending interval. for m/c - divide by 10
//g* - maximum wind per sending interval. for m/c - divide by 10
//i* - device number
 
bool SendToWindyApp() { // send info to http://windy.app/
  
  WiFiClient client;
  HTTPClient http; //must be declared after WiFiClient for correct destruction order, because used by http.begin(client,...)
  String getData, Link;
  unsigned long time;
  
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
     Link = "http://windyapp.co/apiV9.php?method=addCustomMeteostation&secret=" + WindyAppSecret + "&i=" + WindyAppID +"&";
     getData = "d5=" + String(calDirection * 1024 / 360) + "&m=" + String(WindMin *10, 0) + "&a=" + String(WindAvr*10, 0) + "&g=" + String(WindMax *10, 0);
     debugln(Link + getData);
     http.begin(client, Link + getData);    //Specify request destination
     int httpCode = http.GET();             //Send the request
     if (httpCode > 0) {                    //Check the returning code
       String payload = http.getString();   //Get the request response payload
       debugln(payload);             //Print the response payload
       if (mqttClient.connected() && (payload.indexOf("success") == -1))
         mqttClient.publish(MQTT::Publish(MQTT_TOPIC"/debug",payload).set_retain().set_qos(1));
     }
     http.end();   //Close connection
   } else  {
      debugln("wi-fi connection failed");
      return false; // fail;
   }
    
  return true; //done
}
