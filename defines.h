
// MQTT
#define MQTT_TOPIC      "BicaStation"                            // mqtt topic (Must be unique for each device)
#define MQTT_TOPICm     "BicaStation/m"
                         
// basta publicar o valor de SpeedRatio no tópico BicaStation/SpeedRatio para ajustar a velocidade do vento
#define MQTT_TOPICo     "BicaStation/SpeedRatio"                 // mqtt topic to performa wind speed calibration
char mqtt_server[30];
char mqtt_port[6];
char mqtt_user[20];
char mqtt_pass[20];
#define FirmwareURL "http://server/firmware.bin"                //URL of firmware file for http OTA update by secret MQTT command "flash" 
//char kc_wind[4]      = "100";
char vaneMaxADC[5]     = "1024";                                // ADC range for input voltage 0..1V
char  buf_vo[5]        = "0.0";
char  buf_sr[5]        = "1.0";
float vaneOffset       = 180.0;                                 // Offset for wind direction calibration
float SpeedRatio       = 1.0;                                   // Offset for wind speed calibration

// WiFi portal configuration
#define VERSION     "v1.01"
const char compile_date[] = __DATE__ " " __TIME__;
#define NameAP      "WindStationAP"
#define PasswordAP  "87654321"

// WindGuru
char windguru_uid[30]  = "figueira";
char windguru_pass[20] = "29011976";
long WindGuruTimer     = 250;                                    // contador de tempo para enviar dados ao WindGuru

// Windy.com
static String WindyComApiKey  = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaSI6NDcyOTM2LCJpYXQiOjE1OTUwMzI4NjF9.-ibzq7y8G6QDC68VWaiHHJ1EELBSb1iXnxp5tig7Oh4";
unsigned long WindyComTimer   = 250;                              // contador de tempo para enviar dados ao Windy.com
unsigned long SendDataToSites = 250;

// Wind.app
static String WindyAppSecret = "YOUR_SECRET";
static String WindyAppID = "YOUR_ID"; 

// OFFSET PARA A BIRUTA
//#define Offset 0;

// ANEMOMETER PARAMETERS
volatile byte rpmcount      = 0;        // count signals
volatile unsigned long last_micros;
unsigned long timeold       = 0;
unsigned long timemeasure   = 10;       // seconds
//int countThing            = 0;
float rpm, rps;                         // frequencies
float radius                = 0.075;    // meters - measure of the lenght of each the anemometer wing
float linear_velocity       = 0;        // wind linear velocity in m/s
float linear_velocity_kmh   = 0;        // wind linear velocity in km/h
float linear_velocity_knots = 0;        // wind linear velocity in knots
float omega                 = 0;        // angular velocity in rad/seg
